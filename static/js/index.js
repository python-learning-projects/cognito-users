function userLoggedIn() {
    return window.localStorage.getItem('jwt_token') !== null;
}

$(function() {
    if (userLoggedIn()) {
        $.ajax({
            url: "/current-user-info",
            dataType : 'json',
            beforeSend : function(xhr) {
                // set header if JWT is set
                if (window.localStorage['jwt_token']) {
                    xhr.setRequestHeader("Authorization", "Basic " + window.localStorage['jwt_token']);
                }
            },
            error : function(jqXhr, textStatus, errorMessage) {
                DevExpress.ui.notify("User info loading error", "error", 6000);
                console.error("GET /current-user-info\nStatus: " + textStatus + "\n" +
                    "Error message: " + errorMessage)
            },
            success: function(data) {
                DevExpress.ui.notify("data.userName: " + data.userName, "success", 6000);
                console.log("data.userName: " + data.userName)
                $("#user-dropdown").dxDropDownButton({
                    items: [
                        { id: 1, name: "Profile", icon: "user" },
                        { id: 3, name: "Logout", icon: "runner" }
                    ],
                    splitButton: true,
                    onButtonClick: function(e) {
                        DevExpress.ui.notify("Go to " + e.component.option("text") + "'s profile", "success", 600);
                    },
                    onItemClick: function(e) {
                        DevExpress.ui.notify(e.itemData.name, "success", 600);
                    },
                    text: data.userName,
                    icon: "images/gym/coach-woman.png",
                    displayExpr: "name",
                    keyExpr: "id",
                    useSelectMode: false
                });
            }
        });
    }

    const menuData = [{
        id: '1',
        text: 'Video Players',
        items: [{
            id: '1_1',
            text: 'HD Video Player',
        }, {
            id: '1_2',
            text: 'SuperHD Video Player',
        }],
    }, {
        id: '2',
        text: 'Administration',
        items: [{
            id: '2_2',
            text: 'Users',
            targetPath: '/users',
        }, {
            id: '2_3',
            text: 'SuperLED 50',
        }],
    }, {
        id: '3',
        text: 'Monitors',
        items: [{
            id: '3_1',
            text: '19"',
            items: [{
                id: '3_1_1',
                text: 'DesktopLCD 19',
                icon: 'images/products/10.png',
                price: 160,
            }],
        }, {
            id: '3_2',
            text: '21"',
            items: [{
                id: '3_2_1',
                text: 'DesktopLCD 21',
                icon: 'images/products/12.png',
                price: 170,
            }, {
                id: '3_2_2',
                text: 'DesktopLED 21',
                icon: 'images/products/13.png',
                price: 175,
            }],
        }],
    }, {
        id: '4',
        text: 'Projectors',
        items: [{
            id: '4_1',
            text: 'Projector Plus',
            icon: 'images/products/14.png',
            price: 550,
        }, {
            id: '4_2',
            text: 'Projector PlusHD',
            icon: 'images/products/15.png',
            price: 750,
        }],
    }];

    $('#main-menu-toolbar').dxToolbar({
        height: 40,
        items: [{
            widget: 'dxMenu',
            options: {
                dataSource: menuData,
                text: 'Back',
                onItemClick(data) {
                    if (typeof data.itemData.targetPath === 'undefined')
                        return;
                    window.location.href = data.itemData.targetPath;
                },
            },
            location: 'before'
        }, {
            widget: 'dxButton',
            options: {
                stylingMode: "contained",
                text: "Login",
                type: "default",
                width: 120,
                // height: 30,
                visible: !userLoggedIn(),
                onClick: function () {
                    window.location = '/sign_in';
                }
            },
            location: 'after',
            cssClass: 'se-login-button',
        }, {
            html: '<div id="user-dropdown"></div>',
            location: 'after',
        }, {
            widget: 'dxButton',
            options: {
                stylingMode: "contained",
                text: "Logout",
                type: "default",
                width: 120,
                visible: userLoggedIn(),
                onClick: function () {
                    window.localStorage.removeItem('jwt_token');
                    window.location.reload(false);
                }
            },
            location: 'after',
            cssClass: 'se-logout-button',
        }]
    });
});

from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

from tests.element import LabelElement, InputElement, ButtonElement, MenuItemElement, MenuElement


class BasePage(object):
    """Base class to initialize the base page that will be called from all
    pages"""

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 2)
        self.main_menu = MenuElement(self.driver, (By.CSS_SELECTOR, "#main-menu-toolbar .dx-menu"))
        self.header = LabelElement(self.driver, (By.CSS_SELECTOR, 'h1'))


class MyTestSecretPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.message_label = LabelElement(driver, (By.ID, 'rootDivId'))

    def wait_until_loaded(self):
        self.wait.until(lambda _: self.message_label.text != '')


class CognitoLoginPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.username_input = InputElement(driver, (By.ID, "signInFormUsername"))
        self.password_input = InputElement(driver, (By.ID, "signInFormPassword"))
        self.submit_button = ButtonElement(driver, (By.CSS_SELECTOR, '[name="signInSubmitButton"]'))

    def wait_until_loaded(self):
        sleep(0.1)


class MainPage(BasePage):
    pass


class UsersPage(BasePage):
    pass


class Browser:
    def __init__(self, driver: WebDriver) -> None:
        super().__init__()
        self.driver = driver

    def log_in(self):
        self.driver.get("http://127.0.0.1:5000/")
        sleep(0.5)
        self.driver.find_element_by_css_selector(".se-login-button").click()
        login_page = CognitoLoginPage(self.driver)
        login_page.wait_until_loaded()
        login_page.username_input.text = "test"
        login_page.password_input.text = "Test123!"
        sleep(1)
        login_page.submit_button.click()

    def get_current_page(self, page_object_class):
        result = page_object_class(self.driver)
        sleep(1)
        return result

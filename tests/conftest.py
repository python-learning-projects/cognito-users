import pytest
from selenium.webdriver.remote.webdriver import WebDriver

from tests.page import Browser


@pytest.fixture
def browser(driver: WebDriver):
    return Browser(driver)
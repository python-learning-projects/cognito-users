from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class BaseElement(object):
    def __init__(self, driver: WebDriver, web_element_locator):
        self.driver = driver
        self.web_element_locator = web_element_locator

    def find_displayed_element(self, web_element_locator):
        elements = self.driver.find_elements(*web_element_locator)
        # visible_elements = [x for x in elements if x.is_displayed()]
        # visible_elements_2 = filter(lambda x: x.is_displayed(), elements)
        # visible_elements_3 = filter(element_is_displayed, elements)
        displayed_elements = list(filter(lambda x: x.is_displayed(), elements))
        assert len(displayed_elements) == 1
        return displayed_elements[0]


class LabelElement(BaseElement):
    @property
    def text(self):
        return self.driver.find_element(*self.web_element_locator).text


class InputElement(BaseElement):

    @property
    def text(self):
        raise NotImplementedError()

    @text.setter
    def text(self, value):
        web_element = self.find_displayed_element(self.web_element_locator)
        web_element.send_keys(value)


class ButtonElement(BaseElement):

    def click(self):
        return self.find_displayed_element(self.web_element_locator).click()


class MenuItemElement(object):
    def __init__(self, driver: WebDriver, web_element_locators_chain):
        self.driver = driver
        self.web_element_locators_chain = web_element_locators_chain

    @property
    def text(self):
        return self._find_element_by_chain().text

    def click(self):
        return self._find_element_by_chain().click()

    @property
    def submenu_items(self):
        submenu_item_selector = ".dx-context-menu > .dx-submenu > ul > li"
        submenu_elements = self.driver.find_elements(By.CSS_SELECTOR, submenu_item_selector)
        result = []
        for i in range(len(submenu_elements)):
            locators_chain = \
                [(By.CSS_SELECTOR, submenu_item_selector + f':nth-child({i + 1})')]
            result.append(SubMenuItemElement(self.driver, locators_chain))
        return result

    def _find_element_by_chain(self):
        web_element = self.driver
        for locator in self.web_element_locators_chain:
            web_element = web_element.find_element(*locator)
        return web_element


class SubMenuItemElement(object):
    def __init__(self, driver: WebDriver, web_element_locators_chain):
        self.driver = driver
        self.web_element_locators_chain = web_element_locators_chain

    @property
    def text(self):
        return self._find_element_by_chain().text

    def click(self):
        return self._find_element_by_chain().click()

    @property
    def submenu_items(self):
        submenu_selector = "> div > div > ul > li"
        submenu_elements = self._find_element_by_chain().find_elements(By.CSS_SELECTOR, submenu_selector)
        result = []
        for i in range(len(submenu_elements)):
            locators_chain = \
                self.web_element_locators_chain + \
                [(By.CSS_SELECTOR, submenu_selector + f':nth-child({i + 1})')]
            result.append(MenuItemElement(self.driver, locators_chain))
        return result

    def _find_element_by_chain(self):
        web_element = self.driver
        for locator in self.web_element_locators_chain:
            web_element = web_element.find_element(*locator)
        return web_element


class MenuElement(BaseElement):
    @property
    def menu_items(self):
        menu_elements = self.driver.find_element(*self.web_element_locator).find_elements(By.CSS_SELECTOR, "li")
        result = []
        for i in range(len(menu_elements)):
            locators_chain = [
                self.web_element_locator,
                (By.CSS_SELECTOR, f'li:nth-child({i + 1})')]
            result.append(MenuItemElement(self.driver, locators_chain))
        return result

from time import sleep

from selenium.webdriver.remote.webdriver import WebDriver

from tests.element import MenuItemElement
from tests.page import MyTestSecretPage, CognitoLoginPage, MainPage, UsersPage, Browser


def test_login(driver: WebDriver):
    driver.get("http://127.0.0.1:5000/")
    sleep(0.5)
    driver.find_element_by_css_selector(".se-login-button").click()

    login_page = CognitoLoginPage(driver)
    login_page.wait_until_loaded()
    login_page.username_input.text = "test"
    login_page.password_input.text = "Test123!"
    sleep(1)
    login_page.submit_button.click()
    sleep(1)
    secret_link = driver.find_element_by_css_selector('.se-secret-page-link')
    secret_link.click()

    secret_page = MyTestSecretPage(driver)
    secret_page.wait_until_loaded()
    assert secret_page.message_label.text == 'Successfully accessed'


def find_displayed_element(driver, selector):
    elements = driver.find_elements(*selector)
    # visible_elements = [x for x in elements if x.is_displayed()]
    # visible_elements_2 = filter(lambda x: x.is_displayed(), elements)
    # visible_elements_3 = filter(element_is_displayed, elements)
    displayed_elements = list(filter(lambda x: x.is_displayed(), elements))
    assert len(displayed_elements) == 1
    username_input = displayed_elements[0]
    return username_input


def test_no_auth(driver):
    driver.get("http://127.0.0.1:5000/")
    sleep(0.1)

    secret_link = driver.find_element_by_css_selector('.se-secret-page-link')
    secret_link.click()
    secret_page = MyTestSecretPage(driver)
    secret_page.wait_until_loaded()
    assert secret_page.message_label.text == 'No access'


def test_logout(driver: WebDriver):
    driver.get("http://127.0.0.1:5000/")
    sleep(0.5)
    driver.find_element_by_css_selector(".se-login-button").click()
    login_page = CognitoLoginPage(driver)
    login_page.wait_until_loaded()
    login_page.username_input.text = "test"
    login_page.password_input.text = "Test123!"
    sleep(1)
    login_page.submit_button.click()
    sleep(1)

    driver.find_element_by_css_selector(".se-logout-button").click()
    sleep(0.1)

    secret_link = driver.find_element_by_css_selector('.se-secret-page-link')
    secret_link.click()
    secret_page = MyTestSecretPage(driver)
    secret_page.wait_until_loaded()
    assert secret_page.message_label.text == 'No access'


def test_nav_to_users(browser: Browser):
    browser.log_in()

    main_page = browser.get_current_page(MainPage)
    second_element: MenuItemElement = main_page.main_menu.menu_items[1]
    assert second_element.text == 'Administration'
    second_element.click()
    for submenu_item in second_element.submenu_items:
        if submenu_item.text == 'Users':
            submenu_item.click()
            break

    users_page = browser.get_current_page(UsersPage)
    assert users_page.header.text == 'Users'



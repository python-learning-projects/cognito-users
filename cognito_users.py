import boto3
from flask import Flask, render_template, make_response, request, \
    redirect, jsonify

# from flask_login import LoginManager, UserMixin
from flask_awscognito import AWSCognitoAuthentication
from flask_jwt_extended import set_access_cookies, JWTManager

app = Flask(__name__, static_url_path='/')
# app.secret_key = b'moo5iBLj2SEZGOzsHCMg'
# login_manager = LoginManager()
# login_manager.init_app(app)


# class User(UserMixin):
#     pass


# @login_manager.user_loader
# def load_user(user_id):
#     return User.get(user_id)

app.config['AWS_DEFAULT_REGION'] = 'eu-central-1'
app.config['AWS_COGNITO_DOMAIN'] = 'python-courses.auth.eu-central-1.amazoncognito.com'
app.config['AWS_COGNITO_USER_POOL_ID'] = 'eu-central-1_LPWirhtDs'
app.config['AWS_COGNITO_USER_POOL_CLIENT_ID'] = '71kui8msol63a2hfcr4g3jiau8'
app.config['AWS_COGNITO_USER_POOL_CLIENT_SECRET'] = 'pq78eib54sufisj4o2lb6eriqsgnr7rst9bglagjfua0h85d3qt'
app.config['AWS_COGNITO_REDIRECT_URL'] = 'http://localhost:5000/aws_cognito_redirect'
aws_auth = AWSCognitoAuthentication(app)


app.config["JWT_SECRET_KEY"] = "super-secret"  # Change this!
jwt = JWTManager(app)


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/users")
def users():

    return render_template('users.html')


@app.route('/sign_in')
def sign_in():
    return redirect(aws_auth.get_sign_in_url())


@app.route('/aws_cognito_redirect')
def aws_cognito_redirect():
    access_token = aws_auth.get_access_token(request.args)
    return render_template("aws_cognito_redirect.html", access_token=access_token)


@app.route('/test/secret')
# TODO @aws_auth.authentication_required()
def test_secret():
    return render_template("test_secret.html")


@app.route('/test/secret-info')
@aws_auth.authentication_required
def test_secret_info():
    return jsonify({'accessStatus': 'accessed'})


@app.route('/current-user-info')
@aws_auth.authentication_required
def current_user_info():
    h = request.headers.get('Authorization')
    assert h.startswith('Basic ')
    token = h[6:]
    client = boto3.client('cognito-idp', region_name='eu-central-1')
    response = client.get_user(AccessToken=token)
    user_name = response['Username']
    return jsonify({'userName': user_name})


@app.route("/list-users")
def list_users():
    client = boto3.client('cognito-idp', region_name='eu-central-1')

    response = client.list_users(
        UserPoolId='eu-central-1_LPWirhtDs',
        AttributesToGet=[
            'email', 'sub'
        ]
    )

    users_list = []
    for user_dict in response['Users']:
        for user_info in user_dict:
            user_name = user_info["Username"]
            user_email = user_info["Attributes"][1]['Value']
            user_cognito = user_info["Attributes"][0]['Value']
            user_status = user_info['UserStatus']

        users_list.append(User(user_dict["Username"], user_name, user_email, user_cognito, user_status))





    result_html = ''
    for user in users_list:
        result_html += f'<div>{user.display_name}</div>'

    return result_html


@app.route("/login")
def login():
    return render_template('login.html')

class User:

    def __init__(self, display_name) -> None:
        super().__init__()
        self.display_name = display_name


    @property
    def display_name(self):
        return self.display_name
    @property
    def id(self):
        # return self.id
        raise NotImplementedError()

    @property
    def email(self):
        raise NotImplementedError()









